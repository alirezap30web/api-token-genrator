<?php

namespace Alirezap30web\TokenGeneratorTest;

use Alirezap30web\TokenGenerator\TokenGeneratorServiceProvider;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app)
    {
        return [TokenGeneratorServiceProvider::class];
    }
}