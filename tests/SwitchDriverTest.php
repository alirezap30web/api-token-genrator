<?php

namespace Alirezap30web\TokenGeneratorTest;

use Alirezap30web\TokenGenerator\TokenGeneratorFacade;

class SwitchDriverTest extends TestCase
{
    /** @test */
    public function driver_can_be_switched_dynamically()
    {
        $uniqueToken = TokenGeneratorFacade::driver('unique')->generate();
        $cryptoToken = TokenGeneratorFacade::driver('crypto')->generate();
        $this->assertNotEquals($uniqueToken, $cryptoToken);
    }
}