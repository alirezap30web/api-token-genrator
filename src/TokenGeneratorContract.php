<?php

namespace Alirezap30web\TokenGenerator;

interface TokenGeneratorContract {
    public function generate(): string;
}
