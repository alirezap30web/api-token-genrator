<?php

return [

    'default' => env('TOKEN_GENERATOR_DEFAULT', 'basic'),

    'drivers' => [

        'basic' => [
            'driver' => \Alirezap30web\TokenGenerator\Drivers\Basic::class,
            'options' => [
                'length' => 10
            ]
        ],

        'crypto' => [
            'driver' => \Alirezap30web\TokenGenerator\Drivers\Crypto::class,
            'options' => [
                'length' => 10 // length in crypto gets doubled
            ]
        ],

        'unique' => [
            'driver' => \Alirezap30web\TokenGenerator\Drivers\Unique::class,
            'options' => [
                'alg' => 'sha256'
            ]
        ]
    ]
];
