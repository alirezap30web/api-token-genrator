<?php

use Alirezap30web\TokenGenerator\TokenGeneratorContract;
use Alirezap30web\TokenGenerator\TokenGeneratorFacade;
use Alirezap30web\TokenGenerator\TokenGeneratorManager;

if (! function_exists('token')) {
    /**
     * @return TokenGeneratorManager|TokenGeneratorContract
     */
    function token()
    {
        return app(TokenGeneratorFacade::class);
    }
}